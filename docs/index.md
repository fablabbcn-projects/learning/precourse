# Welcome!

This is the documentation for the **Masters in Design for Emergent Futures** precourse.

![](assets/mdef.jpg)

Navigate to the different sections to find:

- [Design tools for digital fabrication](digital_fabrication/introduction.md)
- [Communication](communication/introduction.md)
- [Inputs and Outputs: An introduction to code and electronics](electronics_coding/introduction.md)

!!! Note "Check it out!"
    You can find the source code for this documentation [here](https://gitlab.com/fablabbcn-projects/learning/precourse)


