# Fusion 360

![](assets/fusion.jpg)

- fusion 360 file - download [here](https://a360.co/3oE9sfE)

### Resources

- Official rhino site [here](https://www.rhino3d.com/learn/?keyword=kind:%20rhino_win)
- Command List [here](https://docs.mcneel.com/rhino/5/help/en-us/commandlist/command_list.htm)
