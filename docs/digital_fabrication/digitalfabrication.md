# Intro to digital fabrication

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSt0Ju4RtPyHOyC6frKeuqucCX3gkIwCcvKvp5GJgUHhcHkETrguy8ub47_HIaR7q_rqw7AOxSlYDDa/embed?start=false&loop=false&delayms=3000" frameborder="0" width="760" height="469" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>


### Resources

- Fab: The Coming Revolution on Your Desktop [here](https://www.amazon.com/Fab-Revolution-Desktop-Computers-Fabrication/dp/0465027466)
- Designing Reality [here](https://designingreality.org/)
- Fab City Whitepaper [here](https://fab.city/uploads/whitepaper.pdf)
- The Maker's Manual [here](https://www.amazon.com/Makers-Manual-Practical-Industrial-Revolution/dp/145718592X)
