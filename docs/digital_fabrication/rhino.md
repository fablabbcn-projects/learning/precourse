# Rhino

![](assets/Rhino.jpg)

- Rhino5 & Rhino6 file - download [here](assets/intro.zip)

### Resources

- Official rhino site [here](https://www.rhino3d.com/learn/?keyword=kind:%20rhino_win)
- Command List [here](https://docs.mcneel.com/rhino/5/help/en-us/commandlist/command_list.htm)
