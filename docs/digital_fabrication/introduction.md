# Design tools for digital fabrication

## SHORT DESCRIPTION

The bootcamp is a practical and intensive 3 days online training program and a broad reaching introduction to the Fab Lab environment, and the principals of 2D and 3D design for digital fabrication and understand the fundamentals of digital prototyping in order to give the students tools to allow them to develop their future prototypes.

In this course, we'll use design tools to create and modify a CAD design. We'll begin by using Inskape to create 2D vector drawings.
We'll take that design further using Rhino, and finally we will explore parametric design with tools as Fusion 360.

## MODULES

### Introduction to digital fabrication

An introduction to the Fab Lab BCN and to the basics of digital fabrication. We will present the shared inventory and goals of the Fab Lab Network and set our objectives for the bootcamp of digital  fabrication and local manufacturing.

### Digital Design 2D / 3D

We will take a look of the fundamental skill needed at the lab, the ability to transfer your design intentions into a CAD environment, be it in 3D (ideally) or in 2D. To do so, we will be using the Rhinoceros application, we have found to be a great compromise between power and ease of use, as well as providing a gentle learning curve. We will start working in 2D , as this is all that is necessary to start working with some of the simpler machinery in the lab, such as the laser and the vinyl cutters. We will then progress to 3D as needed and according to each person's abilities.

### Parametric Design

Parametric design is the most widely used modeling process in contemporary architecture and design. The course explores the techniques and tools used in parametric modeling and computational design as a foundation for design optimization. Class session will introduce several parametric design modeling platforms and scripting environments that enable rapid generation of 3D models and enable rapid evaluation of parametrically-driven design alternatives.

### Fabrication simulation and Digital prototying

By simulating and validating the real-world performance of a product design digitally, manufacturers often can reduce the number of physical prototypes they need to create before a product can be manufactured, reducing the cost and time needed for physical prototyping.

Digital Prototyping changes the traditional product development cycle from design>build>test>fix to design>analyze>test>build.

## TECHNICAL REQUIREMENTS

### Laptop Requirements

All the students are required to bring their own laptop respecting the following technical requirements:

- Processor: Computer with Intel i5 or i7 processor or AMD Equivalent
- Ram: =< 8GB
- Hard Disk: 200GB + HDD
- Operating System: Windows 10, 8.1 or 7 SP2  - 64 BIT

!!! Warning
    If you have an Apple computer, it is required that you install Windows on Boot Camp which will perform better than Parallels or VMWare. Please do this prior to your arrival to IAAC).


### Software Requirements

* **2D Design Tools**
     * [Inkscape](https://inkscape.org/en/) *(Free + Opensource Graphic Editor)*
     * [Gimp](https://www.gimp.org/) *(Free + Opensource Image Editor)*

* **3D Design Tools**
    * [Rhinoceros 3D](http://www.rhino3d.com/en/) *(Commercial 3D Cad software)*
    * [Fusion360](https://www.autodesk.com/products/fusion-360/personal) *(Commercial 3D Cad software, free educational license)*
