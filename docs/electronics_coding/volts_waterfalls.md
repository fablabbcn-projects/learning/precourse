# Volts, Watts and waterfalls

## Electricity basics

In this section, we will understand the basics of electricity and how it can help us represent information. We will start with the simplest of the circuits, and infer from it how the information can be stored, managed and sent over to other components.

<div style="text-align:center">
<img src="https://i.imgur.com/jRo3hT6.png" width=600></div>

**Charge**

It's all about **electric charge**. Objects can hold an **electric charge** (either positive or negative), or not (meaning they are neutral). In subatomic particles, **protons hold positive charge**, **electrons hold negative charge** and **neutrons are neutral**. We find these particles in atoms and they define the world as we see it, being the underlying structure of what we understand as **elements** (Hydrogen, Helium, Uranium...). We all know that particles charge go like:

<div style="text-align:center">
<img src="https://cdn.sparkfun.com/r/400-400/assets/a/6/2/4/4/519fb817ce395fff0a000000.png" width=300></div>

In an atom, the core holds protons and neutrons together, while electrons are orbitting around them:

<div style="text-align:center">
<img src="https://cdn.sparkfun.com/assets/3/4/1/a/3/51a65d7bce395f156c000000.png" width=400></div>

**Voltage**

When we **force** electrons to group in a certain area, leaving another area without electrons, we create a **difference in voltage**. This voltage is the relationship between the energy we applied and the electric charge: 
<div style="text-align:center">
_E = V x Charge_
</div><br>

When **two objects have a difference in voltage**, we can say that their electrons will try to jump from one another creating a **current flow**, to balance out the situation and become stable. Voltage is expressed in Volts (V). This voltage can be constant with time, or alternating:

<div style="text-align:center">
<img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse2.mm.bing.net%2Fth%3Fid%3DOIP.m1RMgxYkkPna9Jrx-KnGJgAAAA%26pid%3DApi&f=1" width=400></div>

<div style="text-align:center">
<img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse4.mm.bing.net%2Fth%3Fid%3DOIP.Aa-X7gCjIsLGmqi3M-oVvAHaGP%26pid%3DApi&f=1" width=400></div>

!!! Note "The units"
    Energy is measured in Jules (J), voltage in Volts (V) and Charge in Coulombs (C). Note that all the units that come from the discoverer's name are capitalised!

**Current**

When two objects are subject to a difference voltage, electrons will try to come back to their position. When doing so, we say there is an _electric current_ or just _current_. This movement of electrons inside a material is measured in Amperes (A) or just Amps. If we have alternating voltage, we will have also alternating current (AC), and the same with constant voltage, in which case we will have DC.

<div style="text-align:center">
<img src="https://cdn.sparkfun.com/assets/9/5/6/1/4/519fcd42ce395f804c000000.gif" width=400></div>

**Ohm's law**

For electrons to go from one point to another, when subject to voltage, they will have it more or less difficult to go through. How difficult it is, is called **resistance**, and it's measured in Ohms (Ω). 

<div style="text-align:center">
<p><a href="https://commons.wikimedia.org/wiki/File:Georg_Simon_Ohm3.jpg#/media/File:Georg_Simon_Ohm3.jpg"><img src="https://upload.wikimedia.org/wikipedia/commons/2/2a/Georg_Simon_Ohm3.jpg" alt="Georg Simon Ohm3.jpg"></a><br></p></div>
<p><i>By <a href="https://commons.wikimedia.org/w/index.php?curid=1507336">Wikipedia</a></i></p>

[Georg Ohm](https://en.wikipedia.org/wiki/Georg_Ohm) discovered that voltage (V), resistance (R) and current (I) go with the following formula: 

![](assets/ohm.png)

Meaning that when the resistance is two high, there is almost no current (and when the current is 0, it means we have an _open circuit_).  When the resistance is almost 0, the current can be very big, leading to what we call a _short_.

![](assets/vcr.jpg)

The nice thing about it, is that we can **control this flow** (or lack of it), and then we can make very cool things!

<div style="text-align:center">
<img src="https://cdn.sparkfun.com/assets/a/0/9/4/0/51a52b62ce395f2f25000001.gif" width=400></div><br>

!!! Note "Some reference tutorials"
    - [Electricity basics](https://learn.sparkfun.com/tutorials/what-is-electricity)
    - [Voltage, current and resistance](https://learn.sparkfun.com/tutorials/voltage-current-resistance-and-ohms-law)


## Our new best friends

### The multimeter

A device that combines several measurement functions in one single unit. At it's minimum can measure **Voltage**, **Resistance** and **Current**. It also serves as a debugging tool to check for **[continuity between points](assets/continuity_mode.jpg)** in the circuit.

<div style="text-align:center">
<img src="https://proxy.duckduckgo.com/iu/?u=https%3A%2F%2Fcdn.sparkfun.com%2Fassets%2Flearn_tutorials%2F1%2F01_Multimeter_Tutorial-04.jpg&f=1" width=400></div><br>

!!! Note "References"
    Here are some resources for better understanding the multimeter and learning how to use it:

    - [Sparkfun tutorial](https://learn.sparkfun.com/tutorials/how-to-use-a-multimeter/all)
    - [How to use a multimeter](https://www.sciencebuddies.org/science-fair-projects/references/how-to-use-a-multimeter): this page also contains a summary of the symbols and different forms in which they are represented in diferent multimeters

### The breadboard

It is a prototyping tool we use to **connect electronic components with ease**, so we can make circuits fast and prototype easily:

![](assets/breadboard.png)

!!! Note "References"
	- Another sparkfun [tutorial](https://learn.sparkfun.com/tutorials/how-to-use-a-breadboard), now about breadboards.

## Hands on!

Let's make these first easy circuits and compare:

- Voltages in +/- lines with a multimeter (change USB - Battery) and resistor size
- How bright the LED is

![](assets/circuit_01_batt.png)

![](assets/circuit_01_usb.png)

Now let's integrate a **push button** in our circuit so we can control the led manually.

![](assets/circuit_02_usb.png)

!!! Note "References"
	[Resistor calculator](https://www.calculator.net/resistor-calculator.html?bandnum=5&band1=red&band2=red&band3=black&multiplier=black&tolerance=brown&temperatureCoefficient=brown&type=c&x=82&y=26) calculate your resistor values.
